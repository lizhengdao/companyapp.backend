package com.agrzybow.companyapp.backend.general.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.repository.UserGroupEntityRepository;
import com.agrzybow.companyapp.backend.general.model.UserGroupEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/user-group")
public class UserGroupEntityAdminRestController implements AdminRestControllerInterface<UserGroupEntity, Integer> {
    private UserGroupEntityRepository userGroupEntityRepository;

    @Autowired
    public UserGroupEntityAdminRestController(UserGroupEntityRepository userGroupEntityRepository) {
        this.userGroupEntityRepository = userGroupEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<UserGroupEntity> getAllEntities() {
        return userGroupEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public UserGroupEntity getEntityById(@PathVariable Integer id) {
        return userGroupEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<UserGroupEntity> createEntity(@RequestBody List<UserGroupEntity> newEntity) {
        return userGroupEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public UserGroupEntity editEntity(@RequestBody UserGroupEntity editedEntity) {
        return userGroupEntityRepository.save(editedEntity);
    }
}
