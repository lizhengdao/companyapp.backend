package com.agrzybow.companyapp.backend.general.model.api.client;

import com.agrzybow.companyapp.backend.general.model.dto.UserDTO;
import com.agrzybow.companyapp.backend.general.model.mapper.UserEntityDTOMapper;
import com.agrzybow.companyapp.backend.general.model.repository.LoginLogEntityRepository;
import com.agrzybow.companyapp.backend.general.model.repository.UserEntityRepository;
import com.agrzybow.companyapp.backend.general.model.LoginLogEntity;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api/client/user")
public class UserEntityClientRestController {
    private Long timeToChangePassword = 7776000000L;

    private UserEntityRepository userEntityRepository;
    private LoginLogEntityRepository loginLogEntityRepository;
    private UserEntityDTOMapper mapper;

    @Autowired
    public UserEntityClientRestController(UserEntityRepository userEntityRepository, LoginLogEntityRepository loginLogEntityRepository, UserEntityDTOMapper mapper) {
        this.userEntityRepository = userEntityRepository;
        this.loginLogEntityRepository = loginLogEntityRepository;
        this.mapper = mapper;
    }

    @GetMapping("/display/{username}")
    public List<UserDTO> getAllEntities(@PathVariable String username, @RequestParam(required = false) Long newerThan) {
        if (newerThan != null) {
            return mapper.entitiesToDtos(userEntityRepository.findByUsernameNotAndModifyDateGreaterThan(username, newerThan).orElse(new ArrayList<>()));
        }
        return mapper.entitiesToDtos(userEntityRepository.findByUsernameNot(username).orElse(new ArrayList<>()));

    }

    @PreAuthorize("#id == authentication.name")
    @GetMapping("/display/{id}/login")
    public UserDTO login(@PathVariable String id) {
        UserEntity userEntity = userEntityRepository.findById(id).orElse(null);
        if (userEntity != null) {
            LoginLogEntity loginLogEntity = new LoginLogEntity();
            loginLogEntity.setModifyBy(userEntity);
            loginLogEntity.setModifyDate(System.currentTimeMillis());
            loginLogEntityRepository.save(loginLogEntity);
        }
        return mapper.entityToDto(userEntity);
    }

    @PreAuthorize("#id == authentication.name")
    @GetMapping("/display/{id}/password-expired")
    public Boolean checkIfPasswordExpired(@PathVariable String id) {
        AtomicReference<Boolean> result = new AtomicReference<>(false);
        userEntityRepository.findById(id).ifPresent(userEntity -> result.set(userEntity.getPasswordExpiredAt() <= System.currentTimeMillis()));
        return result.get();
    }

    @PreAuthorize("#id == authentication.name")
    @PostMapping("/display/{id}/change-password")
    public Boolean changePassword(@PathVariable String id, @RequestParam String newPassword) {
        AtomicReference<Boolean> result = new AtomicReference<>(false);
        userEntityRepository.findById(id).ifPresent(userEntity -> {
            userEntity.setPassword(newPassword);
            userEntity.setPasswordExpiredAt(System.currentTimeMillis() + timeToChangePassword);
            userEntityRepository.save(userEntity);
            result.set(true);
        });
        return result.get();
    }

    @PreAuthorize("#username == authentication.name")
    @PostMapping("/add/{username}/save-token")
    public String saveToken(@PathVariable String username, @RequestParam String token) {
        AtomicReference<String> result = new AtomicReference<>(null);
        userEntityRepository.findById(username).ifPresent(userEntity -> {
                    userEntity.setToken(token);
                    userEntityRepository.save(userEntity);
                    result.set(token);
                }
        );
        return result.get();
    }
}
