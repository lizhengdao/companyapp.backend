package com.agrzybow.companyapp.backend.general.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.repository.UserPermissionEntityRepository;
import com.agrzybow.companyapp.backend.general.model.UserPermissionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/user-permission")
public class UserPermissionEntityAdminRestController implements AdminRestControllerInterface<UserPermissionEntity, Integer> {
    private UserPermissionEntityRepository userPermissionEntityRepository;

    @Autowired
    public UserPermissionEntityAdminRestController(UserPermissionEntityRepository userPermissionEntityRepository) {
        this.userPermissionEntityRepository = userPermissionEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<UserPermissionEntity> getAllEntities() {
        return userPermissionEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public UserPermissionEntity getEntityById(@PathVariable Integer id) {
        return userPermissionEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<UserPermissionEntity> createEntity(@RequestBody List<UserPermissionEntity> newEntity) {
        return userPermissionEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public UserPermissionEntity editEntity(@RequestBody UserPermissionEntity editedEntity) {
        return userPermissionEntityRepository.save(editedEntity);
    }
}
