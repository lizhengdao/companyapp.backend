package com.agrzybow.companyapp.backend.mapmanagement.model.repository;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MapSpotCategoryEntityRepository extends JpaRepository<MapSpotCategoryEntity, Integer> {
    List<MapSpotCategoryEntity> findByModifyDateGreaterThan(Long modifyDate);
}
