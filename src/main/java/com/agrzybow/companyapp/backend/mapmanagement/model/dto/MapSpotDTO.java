package com.agrzybow.companyapp.backend.mapmanagement.model.dto;

import com.agrzybow.companyapp.backend.general.model.dto.UserDTO;
import lombok.Data;

@Data
public class MapSpotDTO {
    private Integer id;
    private Double lat;
    private Double lng;
    private String name;
    private String description;
    private Boolean verified;
    private MapSpotCategoryDTO category;
    private Long modifyDate;
    private UserDTO modifyBy;
}
