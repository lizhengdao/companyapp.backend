package com.agrzybow.companyapp.backend.mapmanagement.model.api.client;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.backend.mapmanagement.model.dto.MapSpotDTO;
import com.agrzybow.companyapp.backend.mapmanagement.model.mapper.MapSpotEntityDTOMapper;
import com.agrzybow.companyapp.backend.mapmanagement.model.repository.MapSpotEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/client/map-spot")
public class MapSpotEntityClientRestController {
    private MapSpotEntityRepository mapSpotEntityRepository;
    private MapSpotEntityDTOMapper mapper;

    @Autowired
    public MapSpotEntityClientRestController(MapSpotEntityRepository mapSpotEntityRepository, MapSpotEntityDTOMapper mapper) {
        this.mapSpotEntityRepository = mapSpotEntityRepository;
        this.mapper = mapper;
    }

    @GetMapping("/display")
    public List<MapSpotDTO> getAllEntities(@RequestParam(required = false) Long newerThan) {
        if(newerThan != null) {
            return mapper.entitiesToDtos(mapSpotEntityRepository.findByModifyDateGreaterThan(newerThan));
        }
        return mapper.entitiesToDtos(mapSpotEntityRepository.findAll());
    }

    @PreAuthorize("#username == authentication.name")
    @PostMapping("/add")
    public MapSpotDTO saveEntity(@RequestParam String username, @RequestBody MapSpotDTO mapSpotDTO) {
        MapSpotEntity mapSpotEntity = mapper.dtoToEntity(mapSpotDTO);
        MapSpotEntity saved = mapSpotEntityRepository.save(mapSpotEntity);
        MapSpotDTO mapped = mapper.entityToDto(saved);
        return mapped;
    }
}
