package com.agrzybow.companyapp.backend.mapmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.backend.mapmanagement.model.repository.MapSpotEntityRepository;
import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/map-spot")
public class MapSpotEntityAdminRestController implements AdminRestControllerInterface<MapSpotEntity, Integer> {
    private MapSpotEntityRepository mapSpotEntityRepository;

    @Autowired
    public MapSpotEntityAdminRestController(MapSpotEntityRepository mapSpotEntityRepository) {
        this.mapSpotEntityRepository = mapSpotEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<MapSpotEntity> getAllEntities() {
        return mapSpotEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public MapSpotEntity getEntityById(@PathVariable Integer id) {
        return mapSpotEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<MapSpotEntity> createEntity(@RequestBody List<MapSpotEntity> newEntity) {
        return mapSpotEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public MapSpotEntity editEntity(@RequestBody MapSpotEntity editedEntity) {
        return mapSpotEntityRepository.save(editedEntity);
    }
}
