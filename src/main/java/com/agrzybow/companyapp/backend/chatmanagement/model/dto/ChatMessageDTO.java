package com.agrzybow.companyapp.backend.chatmanagement.model.dto;

import com.agrzybow.companyapp.backend.general.model.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ChatMessageDTO {
    private Integer id;
    private String messageText;
    private UserDTO userEntityReceiver;
    private Long modifyDate;
    private UserDTO modifyBy;
}
