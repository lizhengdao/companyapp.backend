package com.agrzybow.companyapp.backend.postmanagement.model.mapper;

import com.agrzybow.companyapp.backend.postmanagement.model.PostEntity;
import com.agrzybow.companyapp.backend.postmanagement.model.dto.PostDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface PostEntityDTOMapper {
    List<PostDTO> entitiesToDtos(List<PostEntity> postEntities);
}
