package com.agrzybow.companyapp.backend.inboxmanagement.model.dto;

import com.agrzybow.companyapp.backend.general.model.dto.UserDTO;
import lombok.Data;

@Data
public class InboxItemDTO {
    private Integer id;
    private String title;
    private String messageText;
    private Boolean readByReceiver;
    private Long modifyDate;
    private UserDTO modifyBy;
}
