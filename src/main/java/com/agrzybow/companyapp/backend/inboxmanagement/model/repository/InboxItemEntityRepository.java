package com.agrzybow.companyapp.backend.inboxmanagement.model.repository;

import com.agrzybow.companyapp.backend.inboxmanagement.model.InboxItemEntity;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface InboxItemEntityRepository extends JpaRepository<InboxItemEntity, Integer> {
    Optional<List<InboxItemEntity>> findByReceiverAndModifyDateGreaterThan(UserEntity receiver, Long newerThan);
    Optional<List<InboxItemEntity>> findByReceiver(UserEntity receiver);
}
