package com.agrzybow.companyapp.backend.inboxmanagement.model;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import com.agrzybow.companyapp.backend.general.util.UserEntitySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "InboxItem")
public class InboxItemEntity {
    public static final String ID_FIELD_NAME = "id";
    public static final String TITLE_FIELD_NAME = "title";
    public static final String MESSAGE_TEXT_FIELD_NAME = "messageText";
    public static final String READ_BY_RECEIVER_FIELD_NAME = "readByReceiver";
    public static final String RECEIVER_FIELD_NAME = "receiver";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Size(min = 1)
    @Column(name = TITLE_FIELD_NAME, nullable = false)
    private String title;

    @NotNull
    @Size(min = 1)
    @Column(name = MESSAGE_TEXT_FIELD_NAME, nullable = false, columnDefinition = "TEXT")
    private String messageText;

    @NotNull
    @Column(name = READ_BY_RECEIVER_FIELD_NAME, nullable = false)
    private Boolean readByReceiver;

    @NotNull
    @ManyToOne
    @JoinColumn(name = RECEIVER_FIELD_NAME, nullable = false)
    @JsonSerialize(using = UserEntitySerializer.class)
    private UserEntity receiver;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    @JsonSerialize(using = UserEntitySerializer.class)
    private UserEntity modifyBy;
}
