package com.agrzybow.companyapp.backend.inboxmanagement.model.api.client;

import com.agrzybow.companyapp.backend.inboxmanagement.model.dto.InboxItemDTO;
import com.agrzybow.companyapp.backend.inboxmanagement.model.mapper.InboxItemEntityDTOMapper;
import com.agrzybow.companyapp.backend.inboxmanagement.model.repository.InboxItemEntityRepository;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api/client/inbox-item")
public class InboxItemEntityClientRestController {
    private InboxItemEntityRepository inboxItemEntityRepository;
    private InboxItemEntityDTOMapper mapper;

    @Autowired
    public InboxItemEntityClientRestController(InboxItemEntityRepository inboxItemEntityRepository, InboxItemEntityDTOMapper mapper) {
        this.inboxItemEntityRepository = inboxItemEntityRepository;
        this.mapper = mapper;
    }

    @PreAuthorize("#username == authentication.name")
    @GetMapping("/{username}/display")
    public List<InboxItemDTO> getAllEntities(@PathVariable String username, @RequestParam(required = false) Long newerThan) {
        if(newerThan != null) {
            return mapper.entitiesToDtos(inboxItemEntityRepository.findByReceiverAndModifyDateGreaterThan(UserEntity.builder().username(username).build(), newerThan).orElse(new ArrayList<>()));
        }
        return mapper.entitiesToDtos(inboxItemEntityRepository.findByReceiver(UserEntity.builder().username(username).build()).orElse(new ArrayList<>()));
    }

    @PreAuthorize("#username == authentication.name")
    @PostMapping("/{username}/display/{id}/mark-as-read")
    public InboxItemDTO markAsRead(@PathVariable String username, @PathVariable Integer id) {
        AtomicReference<InboxItemDTO> result = new AtomicReference<>();
        inboxItemEntityRepository.findById(id).ifPresent(inboxItemEntity -> {
            inboxItemEntity.setReadByReceiver(true);
            result.set(mapper.entityToDto(inboxItemEntityRepository.save(inboxItemEntity)));
        });
        return result.get();
    }
}
