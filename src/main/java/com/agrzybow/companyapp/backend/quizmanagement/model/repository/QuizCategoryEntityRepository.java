package com.agrzybow.companyapp.backend.quizmanagement.model.repository;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizCategoryEntityRepository extends JpaRepository<QuizCategoryEntity, Integer> {
}
