package com.agrzybow.companyapp.backend.quizmanagement.model.dto;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import lombok.Data;

@Data
public class QuizUserAnswerDTO {
    private Integer id;
    private QuizAnswerDTO answer;
    private QuizQuestionDTO question;
    private UserEntity modifyBy;
    private Long modifyDate;
}
