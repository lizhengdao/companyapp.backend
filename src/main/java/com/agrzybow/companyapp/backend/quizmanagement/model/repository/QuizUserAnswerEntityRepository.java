package com.agrzybow.companyapp.backend.quizmanagement.model.repository;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizUserAnswerEntity;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface QuizUserAnswerEntityRepository extends JpaRepository<QuizUserAnswerEntity, Integer> {
    @Query("SELECT qua FROM QuizUserAnswerEntity qua WHERE qua.modifyBy = :username AND :currentTimeMilliseconds - 86400000L <= qua.modifyDate")
    Optional<QuizUserAnswerEntity> findUserQuizAnswerInLast24Hours(@Param("username") UserEntity userEntity, @Param("currentTimeMilliseconds") Long currentTimeMilliseconds);
}
