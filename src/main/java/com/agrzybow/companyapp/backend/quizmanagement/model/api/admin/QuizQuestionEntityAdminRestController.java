package com.agrzybow.companyapp.backend.quizmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizQuestionEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizQuestionEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/quiz-question")
public class QuizQuestionEntityAdminRestController implements AdminRestControllerInterface<QuizQuestionEntity, Integer> {
    private QuizQuestionEntityRepository quizQuestionEntityRepository;

    @Autowired
    public QuizQuestionEntityAdminRestController(QuizQuestionEntityRepository quizQuestionEntityRepository) {
        this.quizQuestionEntityRepository = quizQuestionEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<QuizQuestionEntity> getAllEntities() {
        return quizQuestionEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public QuizQuestionEntity getEntityById(@PathVariable Integer id) {
        return quizQuestionEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<QuizQuestionEntity> createEntity(@RequestBody List<QuizQuestionEntity> newEntity) {
        return quizQuestionEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public QuizQuestionEntity editEntity(@RequestBody QuizQuestionEntity editedEntity) {
        return quizQuestionEntityRepository.save(editedEntity);
    }
}
