package com.agrzybow.companyapp.backend.quizmanagement.model.mapper;


import com.agrzybow.companyapp.backend.quizmanagement.model.QuizAnswerEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.dto.QuizAnswerDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = QuizQuestionEntityDTOMapper.class)
public interface QuizAnswerEntityDTOMapper {
    List<QuizAnswerEntity> dtosToEntities(List<QuizAnswerDTO> quizAnswerDTOs);
    List<QuizAnswerDTO> entitiesToDtos(List<QuizAnswerEntity> quizAnswerEntities);
}
