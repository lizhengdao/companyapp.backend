package com.agrzybow.companyapp.backend.quizmanagement.model.dto;

import lombok.Data;

@Data
public class QuizQuestionDTO {
    private Integer id;
    private String questionText;
    private String hint;
}
