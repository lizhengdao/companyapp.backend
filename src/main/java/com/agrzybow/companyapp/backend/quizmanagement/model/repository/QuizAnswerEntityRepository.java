package com.agrzybow.companyapp.backend.quizmanagement.model.repository;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizAnswerEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizQuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface QuizAnswerEntityRepository extends JpaRepository<QuizAnswerEntity, Integer> {
    Optional<List<QuizAnswerEntity>> findByQuestion(QuizQuestionEntity question);
}
