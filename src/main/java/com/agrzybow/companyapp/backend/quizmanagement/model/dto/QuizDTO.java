package com.agrzybow.companyapp.backend.quizmanagement.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class QuizDTO {
    private QuizCategoryDTO category;
    private QuizQuestionDTO question;
    private List<QuizAnswerDTO> answers;
}
